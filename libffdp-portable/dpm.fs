﻿namespace Cworld2000.OR.DPMatching
    type MatchingResult(minCost: double, startIndex: int, endIndex: int) =
        member x.MinCost = minCost
        member x.StartIndex = startIndex
        member x.EndIndex = endIndex

    module BasicDPMatching =
        type Symbol = A | B | C | NA

        let LocalDistance (X:float[]) (Y:float[]) i j =
            let i1 = i - 1
            let j1 = j - 1
            let sx = if i1 < 0 then 0.0 else X.[i1]
            let sy = if j1 < 0 then 0.0 else Y.[j1]
            let s = sx - sy
            s * s

        let CostMatrix (X:float[]) (Y:float[]) =
            let costs = Array2D.create (X.Length + 1) (Y.Length + 1) 0.0
            let symbols = Array2D.create (X.Length + 1) (Y.Length + 1) Symbol.NA
            let acums = Array2D.create (X.Length + 1) (Y.Length + 1) 0.0
            let normd = Array2D.create (X.Length + 1) (Y.Length + 1) 0.0

            for i in 0 .. (X.Length) do
                costs.[i, 0] <- 0.0
            for i in 1 .. (Y.Length) do
                costs.[0, i] <- System.Double.MaxValue

            for i in 1 .. (X.Length) do
                for j in 1 .. (Y.Length) do
                    let di = i - 2
                    let dj = j - 2

                    let p = 
                        if (di < 0) || (dj < 0)
                            then [| System.Double.MaxValue; costs.[i - 1, j - 1] + (LocalDistance X Y i j); System.Double.MaxValue |]
                            else [| 
                                costs.[i - 1, j - 2] + 2.0 * (LocalDistance X Y i (j - 1));
                                costs.[i - 1, j - 1] + (LocalDistance X Y i j);
                                (costs.[i - 2, j - 1] + 2.0 * (LocalDistance X Y (i - 1) j)) 
                            |]

                    costs.[i, j] <- (Array.min p) + (LocalDistance X Y i j)
                     
                    symbols.[i, j] <-
                        if (p.[0] = Array.min p) then Symbol.A
                        else if (p.[1] = Array.min p) then Symbol.B
                        else if (p.[2] = Array.min p) then Symbol.C else Symbol.NA

                    acums.[i, j] <-
                        if (p.[0] = Array.min p) then acums.[i - 1, j - 2] + 3.0
                        else if (p.[1] = Array.min p) then acums.[i - 1, j - 1] + 2.0
                        else acums.[i - 2, j - 1] + 3.0

            for i in 1 .. (X.Length) do
                for j in 1 .. (Y.Length) do
                    normd.[i, j] <-
                        if acums.[i, j] = 0.0 then System.Double.MaxValue else costs.[i, j] / acums.[i, j]

            (symbols, normd)

        let rec DoBackTrace (symbols : Symbol[,]) (normalizedCosts : float[,]) i index minDistance =
            let normalizedCost = normalizedCosts.[i, normalizedCosts.GetLength(1) - 1]

            let index = if normalizedCost < minDistance then i else index

            if i < (normalizedCosts.GetLength(0) - 1) then
                (
                    if normalizedCost < minDistance then DoBackTrace symbols normalizedCosts (i + 1) index normalizedCost
                    else DoBackTrace symbols normalizedCosts (i + 1) index minDistance
                )
            else (if normalizedCost < minDistance then (normalizedCost, index) else (minDistance, index))

        let GetIndexes (symbols : Symbol[,]) (normalizedCosts : float[,]) =
            let minDistance = System.Double.MaxValue
            let index = (normalizedCosts.GetLength(1) - 1) / 2 + 1
            let i = index
            DoBackTrace symbols normalizedCosts i index minDistance

        let rec DoObtainPattern (symbols : Symbol[,]) ii jj =
            if(jj > 0) then (
                let ptr =
                    match symbols.[ii, jj] with
                        | Symbol.A -> (ii - 1, jj - 2)
                        | Symbol.B -> (ii - 1, jj - 1)
                        | Symbol.C -> (ii - 2, jj - 1)
                        | _ -> (ii - 1, jj - 1)
                DoObtainPattern symbols (fst ptr) (snd ptr)
            )
            else ii

        let ObtainPattern (symbols : Symbol[,]) (normalizedCosts : float[,]) =
            let indexes = GetIndexes symbols normalizedCosts
            let matchIndexEnd = (snd indexes) - 1
            let ii = (snd indexes)
            let jj = (normalizedCosts.GetLength(1) - 1)
            let matchIndexStart = DoObtainPattern symbols ii jj

            (fst indexes, (matchIndexStart, matchIndexEnd))

        let ExecuteMatching (source:float[]) (target:float[]) =
            let costMatrixes = CostMatrix source target
            let indexes = ObtainPattern (fst costMatrixes) (snd costMatrixes)
            let result = new MatchingResult(fst indexes, fst (snd indexes), snd (snd indexes))
            result

    module DifferedDPMatching =
        let Differential X =
            let result = Array.create ((Array.length X) - 1) 0.0
            for i in 1 .. X.Length - 1 do
                Array.set result (i-1) (X.[i - 1] - X.[i])
            result

        let Sign X =
            let result = Array.create (Array.length X) 0.0
            for i in 0 .. X.Length - 1 do
                Array.set result i (
                    if X.[i] > 0.0 then 1.0 
                        else if X.[i] < 0.0 then -1.0 else 0.0
                )
            result

        let CostMatrix X Y =
            let sX = Sign (Differential X)
            let sY = Sign (Differential Y)
            let result = BasicDPMatching.CostMatrix sX sY
            result

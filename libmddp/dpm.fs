﻿namespace Cworld2000.OR.DPMatching
    open MathNet.Numerics
    open MathNet.Numerics.LinearAlgebra
    module VectorDPMatching =
        type Simbol = A | B | C | NA

        let LocalDistance (X:Vector<float>[]) (Y:Vector<float>[]) i j = 
            MathNet.Numerics.Distance.SSD(X.[i], Y.[j])

        let CostMatrix (X:Vector<float>[]) (Y:Vector<float>[]) =
            let costs = Array2D.create (X.Length) (Y.Length) 0.0
            let simbols = Array2D.create (X.Length) (Y.Length) Simbol.NA
            let acums = Array2D.create (X.Length) (Y.Length) 0.0

            for i in 0 .. (X.Length) - 1 do
                costs.[i, 0] <- 0.0
            for i in 1 .. (Y.Length) - 1 do
                costs.[0, i] <- System.Double.MaxValue

            for i in 1 .. (X.Length) - 1 do
                for j in 1 .. (Y.Length) - 1 do
                    let di = i - 2
                    let dj = j - 2

                    let p = 
                        if (di < 0) || (dj < 0)
                            then [| System.Double.MaxValue; costs.[i - 1, j - 1] + (LocalDistance X Y i j); System.Double.MaxValue |]
                            else [| costs.[i - 1, j - 2] + 2.0 * (LocalDistance X Y i (j - 1)); costs.[i - 1, j - 1] + (LocalDistance X Y i j); (costs.[i - 2, j - 1] + 2.0 * (LocalDistance X Y (i - 1) j)) |]

                    costs.[i, j] <- (Array.min p) + (LocalDistance X Y i j) 
                    simbols.[i, j] <-
                        if (p.[0] = Array.min p) then Simbol.A
                        else if (p.[1] = Array.min p) then Simbol.B
                        else if (p.[2] = Array.min p) then Simbol.C else Simbol.NA
                    acums.[i, j] <-
                        if (p.[0] = Array.min p) then acums.[i - 1, j - 2]
                        else if (p.[1] = Array.min p) then acums.[i - 1, j - 1]
                        else acums.[i - 2, j - 1]

            (simbols, costs, acums)            





﻿using Accord.Audio;
using Accord.Audio.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Brainchild.PAC.Fintech
{
    public class FoundermentalFrequencyEstimator
    {
        public static double[] BasicEstimetor(Signal sourceSignal, int frameSize)
        {
            var sourceSize = sourceSignal.Length;
            var sourceSizeP = GetPower2(sourceSize);
            var source2 = new Signal(sourceSignal.Channels, sourceSizeP, sourceSignal.SampleRate, sourceSignal.SampleFormat);
            for(int ch = 0; ch < sourceSignal.Channels; ch++)
                for (int i = 0; i < sourceSignal.Length; i++)
                    source2.SetSample(ch, i, sourceSignal.GetSample(ch, i));

            var complexSource = source2.ToComplex();
            var window = new RectangularWindow(frameSize);
            for (int i = 0; (i + frameSize) <= sourceSizeP; i++)
            {
                var frameSignal = window.Apply(complexSource, i);
                var frame = frameSignal.GetChannel(0);
                var f = Tools.GetMagnitudeSpectrum(frame);         }

            return null;
        }

        private static int GetPower2(int sourceSize)
        {
            int n = 2;

            while(n < sourceSize)
                n *= 2;
            
            return n;
        }
    }
}

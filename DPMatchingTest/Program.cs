﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cworld2000.OR.DPMatching;

namespace DPMatchingTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var signal = new double[] { 1, 2, 3, 4, 5, 6, 7 };
            var template = new double[] { 1, 2, 3, 4 };
            var template2 = new double[] { 4, 3, 2, 1 };

            var costs = BasicDPMatching.CostMatrix(signal, template);
            var costs2 = BasicDPMatching.CostMatrix(signal, template2);
            var costs3 = BasicDPMatching.CostMatrix(signal, signal);

            var r11 = BasicDPMatching.ExecuteMatching(signal, template);
            var r21 = BasicDPMatching.ExecuteMatching(signal, template2);
            var r31 = BasicDPMatching.ExecuteMatching(signal, signal);
        }
    }
}
